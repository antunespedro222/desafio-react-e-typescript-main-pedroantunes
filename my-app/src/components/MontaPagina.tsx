import React from "react";
import styles from "./MontaPagina.module.scss";

// Monta as páginas simples [titulo e paragrafo]

interface MontaPaginaProps {
  titulo: string;
  texto?: string;
}

function MontaPagina(props: MontaPaginaProps) {
  return (
    <div>
      <h1 className={styles.title}>{props.titulo}</h1>
      <p>
        {props.texto}
      </p>
    </div>
  );
}

export default MontaPagina;
