import React from 'react';
import styles from "./SearchBar.module.scss";

function SearchBar() {
  return (
    <form className={styles.searchBar}>
      <input
            id="search"
            name="search"
            type="text"
            placeholder="Buscar..."
          />
      <button type="submit" />
    </form>
  )
}

export default SearchBar