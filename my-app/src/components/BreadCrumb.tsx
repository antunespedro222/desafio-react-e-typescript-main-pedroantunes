import React from 'react';
import {Link} from "react-router-dom";
import styles from './BreadCrumb.module.scss';

interface BreadCrumbProps{
    pagina: string;
  }

function BreadCrumb(props: BreadCrumbProps) {
  return (
    <div className={styles.breadcrumb}>
              <Link to="/">
                <svg role="img" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">
                  <title> Ícone com link para Home  </title>
                  <path d="M15.8326 7.59562L14.6903 6.4533L8.40431 0.167326C8.18118 -0.0557753 7.81942 -0.0557753 7.59628 0.167326L1.31028 6.4533L0.167381 7.59623C-0.0518699 7.82324 -0.0456085 8.185 0.18141 8.40425C0.402871 8.61814 0.753946 8.61814 0.975407 8.40425L1.14226 8.23623V15.4285C1.14226 15.7442 1.3981 16 1.71372 16H14.2857C14.6013 16 14.8572 15.7442 14.8572 15.4285V8.23623L15.0246 8.40368C15.2516 8.62293 15.6134 8.61664 15.8326 8.38965C16.0465 8.16819 16.0465 7.81708 15.8326 7.59562ZM9.71409 14.8571H6.28537V10.2855H9.71409V14.8571ZM13.7142 14.8571H10.857V9.71403C10.857 9.39841 10.6011 9.14256 10.2855 9.14256H5.7139C5.39829 9.14256 5.14244 9.39841 5.14244 9.71403V14.8571H2.28518V7.09334L7.99969 1.3788L13.7142 7.09334V14.8571Z" fill="#C4C4C4"/>
                </svg>
              </Link>
              
              <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8" fill="none">
                <g clipPath="url(#clip0_1_310)">
                <path d="M6.11608 3.60396L2.6762 0.164193C2.45738 -0.0547314 2.10261 -0.0547314 1.8839 0.164193C1.66517 0.382921 1.66517 0.737679 1.8839 0.95639L4.92766 4.00006L1.88398 7.04362C1.66525 7.26244 1.66525 7.61716 1.88398 7.83589C2.10271 8.0547 2.45747 8.0547 2.67629 7.83589L6.11617 4.39607C6.22553 4.28665 6.28015 4.1434 6.28015 4.00008C6.28015 3.85668 6.22543 3.71332 6.11608 3.60396Z" fill="#C4C4C4"/>
                </g>
                <defs>
                <clipPath id="clip0_1_310">
                <rect width="8" height="8" fill="white" transform="matrix(1 0 0 -1 0 8)"/>
                </clipPath>
                </defs>
              </svg>

              <p>{props.pagina}</p>
            </div>
  )
}

export default BreadCrumb