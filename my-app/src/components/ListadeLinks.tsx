import React from "react";
import { NavLink } from "react-router-dom";

interface ListadeLinksProps {
  titulo?: string;
  itens: { name: string; link: string }[];
}

const ListBuilder = (props: ListadeLinksProps) => {
  return (
    <ul>
      {/* loop para renderizar todo item dentro de um <li><a> */}
      {props.itens.map((item, i) => {
        return (
          <li key={i}>
            <NavLink
              to={item.link}
              className={({ isActive }) =>
                [isActive ? "active" : null].filter(Boolean).join(" ")
              }
            >
              {item.name}
            </NavLink>
          </li>
        );
      })}
    </ul>
  );
};

function ListadeLinks(props: ListadeLinksProps) {
  const [show, setShow] = React.useState(false);

  return (
    <div>
      <h2 onClick={() => setShow(!show)} className={show ? "show" : "hide"}>
        {props.titulo}
      </h2>
      <ListBuilder itens={props.itens} />
    </div>
  );
}

export default ListadeLinks;
