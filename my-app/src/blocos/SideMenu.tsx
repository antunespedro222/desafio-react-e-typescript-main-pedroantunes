import React from 'react';
import ListadeLinks from '../components/ListadeLinks';

const SideLinks = [
    {name:'Sobre', link:'/sobre'},
    {name:'Formas de Pagamento', link:'/formas-de-pagamento'},
    {name:'Entrega', link:'/entrega'},
    {name:'Troca e Devolução', link:'/troca-e-devolucao'},
    {name:'Segurança e Privacidade', link:'/seguranca-e-privacidade'},
    {name:'Contato', link:'/contato'}
]

function SideMenu() {
  return (
    <ListadeLinks itens={SideLinks} />
  )
}

export default SideMenu