import React from 'react'
import SideMenu from './SideMenu';
import Page from './Page'; 
import styles from "./ContentBlock.module.scss";
import BreadCrumb from '../components/BreadCrumb';


function ContentBlock() {
  return (
        <main>
          
            <BreadCrumb pagina="institucional" />

            <h1>INSTITUCIONAL</h1>

            <div className={styles.maingrid}>
              <SideMenu />
              <Page /> {/* A unica parte que atualiza ao clicar nos links */}
            </div>

        </main>
  )
}

export default ContentBlock