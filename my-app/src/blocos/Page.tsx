import React from "react";
import { Routes, Route } from "react-router-dom";

import Home from "../pages/Home";
import Sobre from "../pages/Sobre";
import Contato from "../pages/Contato";
import Entrega from "../pages/Entrega";
import NotFound404 from "../pages/NotFound404";
import TrocaeDevolucao from "../pages/TrocaeDevolucao";
import FormasdePagamento from "../pages/FormasdePagamento";
import SegurancaePrivacidade from "../pages/SegurancaePrivacidade";

function Page() {
  return (
    <>
      <Routes>
        <Route path="*" element={<NotFound404 />} />
        <Route path="/" element={<Home />} />
        <Route path="/sobre" element={<Sobre />} />
        <Route path="/contato" element={<Contato />} />
        <Route path="/entrega" element={<Entrega />} />
        <Route path="/troca-e-devolucao" element={<TrocaeDevolucao />} />
        <Route path="/formas-de-pagamento" element={<FormasdePagamento />} />
        <Route
          path="/seguranca-e-privacidade"
          element={<SegurancaePrivacidade />}
        />
      </Routes>
    </>
  );
}

export default Page;
