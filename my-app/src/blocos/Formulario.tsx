import React from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import NumberFormat from "react-number-format";
import styles from "./Formulario.module.scss"

  const MontaForm = () => {

    const formik = useFormik({
      initialValues: {
        nome: "",
        email: "",
        cpf: "",
        idade: "",
        telefone: "",
        instagram: "",
        termodeUso: false
      },
      
      validationSchema: yup.object({
        nome: yup
        .string()
        .required("O campo é obrigatório.")
        .matches(/^[A-Za-z ]*$/, "Números não são válidos")
        .min(3, "Mínimo de 3 caracteres"),

        email: yup
          .string()
          .email("E-mail inválido.")
          .required("O campo é obrigatório."),

        cpf: yup
        .string()
        .required("O campo é obrigatório.")
        .matches(/^([0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}|[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2})$/
        ,"Use um CPF válido de 11 digitos"),

        idade: yup
          .string()
          .required("O campo é obrigatório.")
          .min(8,"O número deve ter no mínimo 8 digitos"),
          // .positive("O campo deve ser positivo.")
          // .integer("O campo deve ser um número inteiro."),

        telefone: yup
          .string()
          .required("O campo é obrigatório.")
          .matches(/\(?\b([0-9]{2,3}|0((x|[0-9]){2,3}[0-9]{2}))\)?\s*[0-9]{4,5}[- ]*[0-9]{4}\b/
          ,"Número inválido"),

        instagram: yup
        .string()
        .matches(/^@[A-Za-z0-9._]+$/
        ,"Use um usuário válido com @"),

        termodeUso: yup
        .bool()
        .oneOf([true], "Aceite os termos de uso")
        .required("Campo obrigatório")
      }),
      onSubmit: (values, { resetForm }) => {
        alert(JSON.stringify(values, null, 2));
        resetForm();
      },
    });
    
    return (
      <form className={styles.formularioInstitucional} onSubmit={formik.handleSubmit}>

        <h1 className={styles.title}>PREENCHA O FORMULÁRIO </h1>

        <div>
          <label htmlFor="nome">Nome</label>
          <input
            id="nome"
            name="nome"
            type="text"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.nome}
            placeholder="Seu nome completo"
          />
          {formik.touched.nome && formik.errors.nome ? (
            <div className={styles.warning}>{formik.errors.nome}</div>
          ) : null}
        </div>
        
        <div>
          <label htmlFor="email">E-mail</label>
          <input
            id="email"
            name="email"
            type="email"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
            placeholder="Seu email"
          />
          {formik.touched.email && formik.errors.email ? (
            <div className={styles.warning}>{formik.errors.email}</div>
          ) : null}
        </div>

        <div>
          <label htmlFor="cpf">CPF</label>
          <NumberFormat
            id="cpf"
            name="cpf"
            type="text"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.cpf}
            placeholder="000.000.000-00"
            format= "###.###.###-##"
          />
          {formik.touched.cpf && formik.errors.cpf ? (
            <div className={styles.warning}>{formik.errors.cpf}</div>
          ) : null}
        </div>

        <div>
          <label htmlFor="idade">Data de nascimento</label>
            <NumberFormat
              id="idade"
              name="idade"
              // type="number"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.idade}
              placeholder="00.00.0000"
              format="##.##.####"
              mask="_"
            />
            {formik.touched.idade && formik.errors.idade ? (
              <div className={styles.warning}>{formik.errors.idade}</div>
            ) : null}
        </div>

        <div>
          <label htmlFor="telefone">Telefone</label>
            <NumberFormat
              id="telefone"
              name="telefone"
              // type="number"
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              value={formik.values.telefone}
              placeholder="(+00) 0000 0000"
              format="(+##) ##### ####"
              mask="_"
            />
            {formik.touched.telefone && formik.errors.telefone ? (
              <div className={styles.warning}>{formik.errors.telefone}</div>
            ) : null}
        </div>

        <div>
          <label htmlFor="instagram">Instagram</label>
          <input
            id="instagram"
            name="instagram"
            type="string"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.instagram}
            placeholder="@seuuser"
          />
          {formik.touched.instagram && formik.errors.instagram ? (
            <div className={styles.warning}>{formik.errors.instagram}</div>
          ) : null}
        </div>

        <div>
          <div className={styles.checkbox}>
            <label htmlFor="termodeUso">Declaro que li e aceito</label>
            
            <input 
            id="termodeUso" 
            type="checkbox"
            name="termodeUso"  
            onChange={formik.handleChange}
            />
          </div>
          
          {formik.touched.termodeUso && formik.errors.termodeUso ? (
              <div className={styles.warning}>{formik.errors.termodeUso}</div>
            ) : null}  
        </div>

        <div>
          <button type="submit">Cadastre-se</button>
        </div>

      </form>
    );
  };

  function Formulario() {
    return <MontaForm />;
  }

export default Formulario