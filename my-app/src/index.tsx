import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import "./Index.scss";

import ContentBlock from "./blocos/ContentBlock";
import HeaderBlock from './blocos/HeaderBlock';
import FooterBlock from "./blocos/FooterBlock";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <BrowserRouter>
    <HeaderBlock />
      <ContentBlock />
    <FooterBlock />
  </BrowserRouter>
);
