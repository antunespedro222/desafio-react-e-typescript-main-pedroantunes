import React from 'react';
import MontaPagina from "../components/MontaPagina"

function NotFound404() {
  return (
    <>
      <MontaPagina 
      titulo="404 Page Not Found" 
      texto="Essa página ainda não foi buildada" 
      />
    </>
  )
}

export default NotFound404